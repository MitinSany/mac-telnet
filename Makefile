#
# Copyright (C) 2010 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#
include $(TOPDIR)/rules.mk

PKG_NAME:=mac-telnet
PKG_VERSION:=2015-05-12
PKG_RELEASE=2

PKG_SOURCE:=$(PKG_NAME)-$(PKG_VERSION).tar.bz2
PKG_SOURCE_SUBDIR:=$(PKG_NAME)-$(PKG_VERSION)
PKG_SOURCE_URL:=https://github.com/k3dt/MAC-Telnet.git
PKG_SOURCE_PROTO:=git
PKG_SOURCE_VERSION:=10830cf39ba55e7f913bf9a144cac4f8cd39c6e1

include $(INCLUDE_DIR)/package.mk
include $(INCLUDE_DIR)/nls.mk

define Package/$(PKG_NAME)
	CATEGORY:=Wifi-Party
	TITLE:=MAC-Telnet
	URL:=https://bitbucket.org/MitinSany/mac-telnet
endef

define Package/$(PKG_NAME)/description
	Open source MAC Telnet client and server for connecting to Mikrotik RouterOS routers and Linux machines via MAC address.
endef

define Package/$(PKG_NAME)/postinst
	/etc/init.d/mactelnetd enable
	/etc/init.d/mactelnetd start
endef

define Package/$(PKG_NAME)/prerm
	/etc/init.d/mactelnetd stop
	/etc/init.d/mactelnetd disable
endef

define Build/Compile
	$(MAKE) CC="$(TARGET_CC)" CFLAGS="$(TARGET_CFLAGS)" -C $(PKG_BUILD_DIR)
endef

define Package/mac-telnet/conffiles
	/etc/config/mactelnetd
	/etc/mactelnetd.users
endef

define Package/$(PKG_NAME)/install
	$(INSTALL_DIR) $(1)/etc $(1)/usr/bin $(1)/usr/sbin $(1)/etc/config $(1)/etc/init.d
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/mndp $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/macping $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/mactelnet $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/mactelnetd $(1)/usr/sbin/
	$(INSTALL_CONF) ./files/mactelnetd.config $(1)/etc/config/mactelnetd
	$(INSTALL_CONF) ./files/mactelnetd.users $(1)/etc/mactelnetd.users
	$(INSTALL_BIN) ./files/mactelnetd.init $(1)/etc/init.d/mactelnetd
endef

$(eval $(call BuildPackage,$(PKG_NAME)))
